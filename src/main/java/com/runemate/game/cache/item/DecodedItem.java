package com.runemate.game.cache.item;

import com.runemate.game.cache.io.*;
import java.io.*;

public interface DecodedItem {
    void decode(Js5InputStream stream) throws IOException;
}
