package com.runemate.game.api.hybrid.local;

import com.runemate.game.api.script.annotations.*;

public interface WorldOverview {
    /**
     * Gets the world id (number)
     */
    int getId();

    @OSRSOnly
    String getActivity();

    boolean isMembersOnly();

    boolean isLootShare();

    @OSRSOnly
    boolean isDeadman();

    @OSRSOnly
    boolean isTournament();

    @RS3Only
    boolean isQuickChat();

    @OSRSOnly
    boolean isPVP();

    @OSRSOnly
    boolean isBounty();

    @OSRSOnly
    boolean isHighRisk();

    @OSRSOnly
    boolean isLastManStanding();

    @OSRSOnly
    boolean isSkillTotal500();

    @OSRSOnly
    boolean isSkillTotal750();

    @OSRSOnly
    boolean isSkillTotal1250();

    boolean isSkillTotal1500();

    @OSRSOnly
    boolean isSkillTotal1750();

    boolean isSkillTotal2000();

    @OSRSOnly
    boolean isSkillTotal2200();

    @RS3Only
    @Deprecated
    boolean isSkillTotal2400();

    @RS3Only
    boolean isSkillTotal2600();

    @RS3Only
    boolean isVIP();

    @RS3Only
    boolean isLegacyOnly();

    @RS3Only
    boolean isEoCOnly();

    @OSRSOnly
    boolean isLeague();
}
