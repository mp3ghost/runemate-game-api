package com.runemate.game.api.hybrid.local;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.*;

/**
 * A local-player variable.
 */
public final class Varp {
    private final int index;

    public Varp(final int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public int getValue() {
        if (index < 0) {
            return -1;
        }
        return OpenVarps.getAt(index);
    }

    public int getValue(Transform<Integer> transform) {
        int value = getValue();
        if (value == -1) {
            return -1;
        }
        return transform.transform(value);
    }

    public int getValueOfBitRange(int least_significant_bit, int most_significant_bit) {
        return CommonMath.getBitRange(getValue(), least_significant_bit, most_significant_bit);
        /*
        int value = getValue();
        if (value == -1) {
            return -1;
        }
        return (value >> least_significant_bit) & BIT_MASKS[most_significant_bit - least_significant_bit];*/
    }

    public int getValueOfBit(int index) {
        int value = getValue();
        if (value == -1) {
            return -1;
        }
        return (value >> index) & CommonMath.getBitMask(1);
    }

    @Override
    public String toString() {
        return "Varp " + index;
    }

    @Override
    public boolean equals(Object o) {
        return this == o || o instanceof Varp && index == ((Varp) o).index;
    }

    @Override
    public int hashCode() {
        return index;
    }
}
