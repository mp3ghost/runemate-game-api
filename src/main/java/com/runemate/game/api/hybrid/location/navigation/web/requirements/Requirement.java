package com.runemate.game.api.hybrid.location.navigation.web.requirements;

import java.io.*;

/**
 * Replaced by WebRequirement
 */
@Deprecated
public abstract class Requirement extends WebRequirement {
    public Requirement() {
    }

    public Requirement(int protocol, ObjectInput stream) {
        super(protocol, stream);
    }
}
