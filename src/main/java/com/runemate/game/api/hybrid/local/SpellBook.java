package com.runemate.game.api.hybrid.local;

public interface SpellBook {
    int getId();

    boolean isCurrent();
}
