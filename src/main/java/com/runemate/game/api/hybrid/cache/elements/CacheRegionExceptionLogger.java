package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.game.api.hybrid.location.*;
import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.*;

public class CacheRegionExceptionLogger {
    public static File getErrorDirectory() {
        return new File("errors");
    }

    public static File getErrorDirectory(String game) {
        return new File(getErrorDirectory(), game);
    }

    public static File getErrorDirectory(String game, String subfolder) {
        return new File(getErrorDirectory(game), subfolder);
    }

    public static File getErrorDirectory(String game, Throwable type) {
        return new File(getErrorDirectory(game), type.getClass().getSimpleName());
    }

    public static File getErrorDirectory(String game, String subfolder, Throwable type) {
        return new File(
            subfolder != null ? getErrorDirectory(game, subfolder) : getErrorDirectory(game),
            type.getClass().getSimpleName()
        );
    }

    public static boolean writeException(String game, Throwable error, Coordinate base) {
        return writeException(game, null, error, base);
    }

    public static boolean writeException(
        String game, String subfolderName, Throwable error,
        Coordinate base
    ) {
        return writeException(game, subfolderName, error,
            "Region " + base.getContainingRegionId() + " (" + base.getX() + ", " + base.getY() +
                ')' + ".log"
        );
    }

    public static boolean writeException(
        String game, String subfolderName, Throwable error,
        String fileName
    ) {
        File errors = new File(getErrorDirectory(game, subfolderName, error), fileName);
        if (!errors.getParentFile().exists()) {
            errors.getParentFile().mkdirs();
        }
        if (!errors.exists()) {
            try {
                Files.createFile(errors.toPath());
            } catch (IOException e) {
                return false;
            }
        }
        try (PrintWriter errorlog = new PrintWriter(errors)) {
            error.printStackTrace(errorlog);
        } catch (FileNotFoundException e) {
            return false;
        }
        return true;
    }

    public static boolean emptyExceptionDirectory(String game) {
        File directory = getErrorDirectory(game);
        if (!directory.exists()) {
            return true;
        }
        try {
            Files.walkFileTree(directory.toPath(), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                    throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc)
                    throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}
