package com.runemate.game.api.hybrid.structures;

import com.runemate.game.cache.io.*;
import java.io.*;

public final class Vector3f implements Cloneable {

    public float x, y, z;

    public Vector3f(Js5InputStream packet) throws IOException {
        this.x = packet.readFloat();
        this.y = packet.readFloat();
        this.z = packet.readFloat();
    }

    public Vector3f() {
    }

    public Vector3f(final float x, final float y, final float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3f add(Vector3f vector) {
        this.x += vector.x;
        this.z += vector.z;
        this.y += vector.y;
        return this;
    }

    public Vector3f subtract(Vector3f vector) {
        this.x -= vector.x;
        this.z -= vector.z;
        this.y -= vector.y;
        return this;
    }

    public Vector3f multiply(Quaternion quaternion) {
        final Quaternion asQuat = new Quaternion(this.x, this.z, this.y, 0.0f);
        final Quaternion j = quaternion.clone().invert();
        final Quaternion g = j.multiply(asQuat).multiply(quaternion);
        this.x = g.getI();
        this.z = g.getJ();
        this.y = g.getK();
        return this;
    }

    @Override
    public Vector3f clone() {
        return new Vector3f(x, y, z);
    }
}
