package com.runemate.game.api.hybrid.entities;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.entities.details.*;
import javax.annotation.*;

/**
 * A non-player character
 */
public interface Npc extends Actor, Identifiable {
    /**
     * Gets the npcs current level
     *
     * @return the npcs level, otherwise -1
     */
    int getLevel();

    /**
     * A definition containing a vast collection of data regarding this npcs creation on the world-graph
     *
     * @return The definition, otherwise null
     */
    @Nullable
    NpcDefinition getDefinition();
}
