package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.game.api.hybrid.entities.attributes.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.io.*;

public class CacheParameterDefinition extends IncrementallyDecodedItem {
    private final int id;
    private final boolean osrs;
    private int defaultint;
    //investigate more
    private boolean autodisable = true;
    private String defaultstr;

    public CacheParameterDefinition(int id, boolean osrs) {
        this.id = id;
        this.osrs = osrs;
    }


    @Override
    protected void decode(Js5InputStream stream, int opcode) throws IOException {
        if (1 == opcode) {
            //type = ScriptVarType.method5588(Class665.method1049(packet.g1b()));
            stream.readByte();
        } else if (2 == opcode) {
            defaultint = stream.readInt();
        } else if (4 == opcode) {
            autodisable = false;
        } else if (5 == opcode) {
            defaultstr = stream.readCStyleLatin1String();
        } else if (101 == opcode) {
            //type = (ScriptVarType) Class142.method1704(ScriptVarType.values(), packet.gsmart1or2());
            stream.readReducedShortSmart();
        } else {
            throw new DecodingException(-1);
        }
    }

    public Extended extended() {
        return new Extended();
    }

    public class Extended extends AttributeDefinition {

        private Extended() {
        }

        @Override
        public int getDefaultInt() {
            return defaultint;
        }

        @Override
        public String getDefaultString() {
            return defaultstr;
        }

        @Override
        public String toString() {
            return "AttributeDefinition(default=" + (defaultstr == null ? defaultint : defaultstr) +
                ")";
        }
    }
}



