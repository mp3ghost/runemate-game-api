package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class ItemDefinitionLoader
    extends SerializedFileLoader<com.runemate.game.api.hybrid.cache.elements.CacheItemDefinition> {
    private final boolean osrs;

    public ItemDefinitionLoader(int file, boolean osrs) {
        super(file);
        this.osrs = osrs;
    }

    @Override
    protected com.runemate.game.api.hybrid.cache.elements.CacheItemDefinition construct(
        int entry,
        int file,
        Map<String, Object> arguments
    ) {
        return new CacheItemDefinition(osrs ? file : (entry << 8) + file, osrs);
    }
}
