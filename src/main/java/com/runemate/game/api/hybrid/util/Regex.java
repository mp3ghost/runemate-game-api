package com.runemate.game.api.hybrid.util;

import com.google.common.cache.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public final class Regex {
    private static final Cache<String, Pattern> EQUALS_CACHE =
        CacheBuilder.newBuilder().softValues().expireAfterAccess(10, TimeUnit.MINUTES).build();
    private static final Cache<String, Pattern> EQUALS_MULTIPLE_CACHE =
        CacheBuilder.newBuilder().softValues().expireAfterAccess(10, TimeUnit.MINUTES).build();
    private static final Cache<String, Pattern> CONTAINS_CACHE =
        CacheBuilder.newBuilder().softValues().expireAfterAccess(10, TimeUnit.MINUTES).build();
    private static final Cache<String, Pattern> OTHER_CACHE =
        CacheBuilder.newBuilder().softValues().expireAfterAccess(10, TimeUnit.MINUTES).build();
    private static final Pattern NULL_PATTERN = Pattern.compile("");

    private Regex() {
    }

    /**
     * Gets a regex pattern object that will match strings that are exactly the given string
     *
     * @param string a regular string of text
     * @return a Pattern object
     */
    public static Pattern getPatternForExactString(final String string) {
        if (string == null) {
            return NULL_PATTERN;
        }
        Pattern pattern = EQUALS_CACHE.getIfPresent(string);
        if (pattern != null) {
            return pattern;
        }
        EQUALS_CACHE.put(
            string,
            pattern = Pattern.compile('^' + escapeSpecialCharacters(string) + '$')
        );
        return pattern;
    }

    // TODO better name
    private static String escapeSpecialCharacters(final String string) {
        StringBuilder escaped_string = new StringBuilder();
        for (char c : string.toCharArray()) {
            if (c == '(' || c == ')' || c == '^' || c == '$' || c == '.' || c == '*' || c == '?' ||
                c == '|' || c == '[' || c == '{' || c == '+') {
                escaped_string.append('\\');
            }
            escaped_string.append(c);
        }
        return escaped_string.toString();
    }

    public static Pattern getPatternForContainsString(final String string) {
        if (string == null) {
            return NULL_PATTERN;
        }
        Pattern pattern = CONTAINS_CACHE.getIfPresent(string);
        if (pattern != null) {
            return pattern;
        }
        CONTAINS_CACHE.put(
            string,
            pattern = Pattern.compile(".*" + escapeSpecialCharacters(string) + ".*")
        );
        return pattern;
    }

    public static Pattern getPatternForExactStrings(final String... strings) {
        StringBuilder pattern_builder = new StringBuilder().append('^');
        boolean first = true;
        for (String s : strings) {
            if (first) {
                first = false;
            } else {
                pattern_builder.append('|');
            }
            pattern_builder.append(escapeSpecialCharacters(s));
        }
        String regex = pattern_builder.append('$').toString();
        Pattern pattern = EQUALS_MULTIPLE_CACHE.getIfPresent(regex);
        if (pattern != null) {
            return pattern;
        }
        pattern = Pattern.compile(regex);
        EQUALS_MULTIPLE_CACHE.put(regex, pattern);
        return pattern;
    }

    public static Pattern getPatternContainingOneOf(final String... strings) {
        StringBuilder pattern_builder = new StringBuilder().append('^');
        boolean first = true;
        for (String s : strings) {
            if (first) {
                first = false;
            } else {
                pattern_builder.append('|');
            }
            pattern_builder.append(".*").append(escapeSpecialCharacters(s)).append(".*");
        }
        return Pattern.compile(pattern_builder.append('$').toString());
    }

    public static List<Pattern> getPatternsForExactStrings(final String... strings) {
        List<Pattern> list = new ArrayList<>(strings.length);
        for (String string : strings) {
            list.add(getPatternForExactString(string));
        }
        return list;
    }

    public static List<Pattern> getPatternsForContainsStrings(final String... strings) {
        List<Pattern> list = new ArrayList<>(strings.length);
        for (String string : strings) {
            list.add(getPatternForContainsString(string));
        }
        return list;
    }

    /**
     * Compiles the supplied regex into a {@link java.util.regex.Pattern} and then caches it util it's needed next (compiling regex is slow).
     *
     * @param regex a valid regex pattern
     * @return a Pattern compiled from the regex or retrieved from the cache of previously requested Patterns.
     */
    public static Pattern getPattern(String regex) {
        Pattern pattern = OTHER_CACHE.getIfPresent(regex);
        if (pattern != null) {
            return pattern;
        }
        pattern = Pattern.compile(regex);
        OTHER_CACHE.put(regex, pattern);
        return pattern;
    }
}
