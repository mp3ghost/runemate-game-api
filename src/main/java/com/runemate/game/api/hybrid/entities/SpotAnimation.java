package com.runemate.game.api.hybrid.entities;

import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.entities.details.*;
import javax.annotation.*;

public interface SpotAnimation extends Identifiable, Animable, LocatableEntity {

    @Nullable
    SpotAnimationDefinition getDefinition();
}
