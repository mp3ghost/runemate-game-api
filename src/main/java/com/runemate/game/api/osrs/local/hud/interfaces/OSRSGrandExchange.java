package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.net.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.script.*;
import java.awt.event.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;

public final class OSRSGrandExchange {

    private static final int GE_CONTAINER = 465;

    private static final String CREATE_BUY_OFFER = "Create Buy offer";
    private static final String CREATE_SELL_OFFER = "Create Sell offer";

    private static final Pattern CREATE = Pattern.compile("^Create (Buy|Sell) offer$");

    private static final Pattern SEARCH_TERM =
        Pattern.compile("^What would you like to buy\\? (?<term>.*)\\*$");

    private OSRSGrandExchange() {
    }

    public static boolean collectToBank() {
        final GrandExchange.Screen screen = getOpenedScreen();
        if (screen == GrandExchange.Screen.ACTIVE_BUY_OFFER ||
            screen == GrandExchange.Screen.ACTIVE_SELL_OFFER) {
            final InterfaceComponent collect =
                Interfaces.newQuery().containers(GE_CONTAINER).types(InterfaceComponent.Type.SPRITE)
                    .actions("Collect").results().first();
            return collect != null && collect.interact("Bank") &&
                Execution.delayWhile(collect::isValid, 1200);
        }
        final InterfaceComponent collect =
            Interfaces.newQuery().containers(GE_CONTAINER).types(InterfaceComponent.Type.SPRITE)
                .actions("Collect to bank").results().first();
        return collect != null && collect.interact("Collect to bank") &&
            Execution.delayWhile(collect::isValid, 1200);
    }

    public static boolean collectToInventory() {
        final GrandExchange.Screen screen = getOpenedScreen();
        if (screen == GrandExchange.Screen.ACTIVE_BUY_OFFER ||
            screen == GrandExchange.Screen.ACTIVE_SELL_OFFER) {
            final InterfaceComponent collect =
                Interfaces.newQuery().containers(GE_CONTAINER).types(InterfaceComponent.Type.SPRITE)
                    .actions("Collect").results().first();
            return collect != null && collect.interact("Collect") &&
                Execution.delayWhile(collect::isValid, 1200);
        }
        final InterfaceComponent collect =
            Interfaces.newQuery().containers(GE_CONTAINER).types(InterfaceComponent.Type.SPRITE)
                .actions("Collect to inventory").results().first();
        return collect != null && collect.interact("Collect to inventory") &&
            Execution.delayWhile(collect::isValid, 1200);
    }

    public static boolean close() {
        final InterfaceComponent component =
            Interfaces.newQuery().containers(GE_CONTAINER).types(InterfaceComponent.Type.SPRITE)
                .actions("Close").results().first();
        return component != null && component.interact("Close") &&
            Execution.delayWhile(GrandExchange::isOpen, 1200, 2400);
    }

    public static boolean returnToOverview() {
        if (getOpenedScreen() == GrandExchange.Screen.OVERVIEW) {
            return true;
        }
        final InterfaceComponent back =
            Interfaces.newQuery().containers(GE_CONTAINER).types(InterfaceComponent.Type.SPRITE)
                .actions("Back").grandchildren(false).results().first();
        return back != null && back.interact("Back") &&
            Execution.delayUntil(() -> getOpenedScreen() == GrandExchange.Screen.OVERVIEW, 1200);
    }

    public static boolean abortOffer(GrandExchange.Slot slot) {
        GrandExchange.Screen screen = getOpenedScreen();
        if (!GrandExchange.Screen.OVERVIEW.equals(screen)) {
            returnToOverview();
        }
        screen = getOpenedScreen();
        if (GrandExchange.Screen.OVERVIEW.equals(screen)) {
            InterfaceComponent slotWindow = getComponentForSlot(slot);
            if (slotWindow != null && slotWindow.interact("Abort offer")) {
                return Execution.delayUntil(() -> slot.getOffer() == null, 1200, 1800);
            }
        }
        return false;
    }

    public static boolean newOffer(
        String item, GrandExchange.Offer.Type type, int quantity,
        int price
    ) {
        if (!isOpen() || type == null) {
            return false;
        }
        final GrandExchange.Slot empty = GrandExchange.getUnusedSlots().random();
        if (empty == null) {
            return false;
        }
        final boolean buyOffer = type == GrandExchange.Offer.Type.BUY;
        GrandExchange.Screen screen = getOpenedScreen();
        if (screen == GrandExchange.Screen.ACTIVE_BUY_OFFER ||
            screen == GrandExchange.Screen.ACTIVE_SELL_OFFER) {
            if (!returnToOverview()) {
                return false;
            }
        }
        return buyOffer ? newBuyOffer(empty, item, quantity, price) :
            newSellOffer(empty, item, quantity, price);
    }

    private static boolean newBuyOffer(
        GrandExchange.Slot slot, String itemName, int quantity,
        int price
    ) {
        GrandExchange.Screen screen = getOpenedScreen();
        if (screen == GrandExchange.Screen.SETUP_SELL_OFFER) {
            if (!returnToOverview()) {
                return false;
            }
            screen = GrandExchange.Screen.OVERVIEW;
        }
        if (screen == GrandExchange.Screen.OVERVIEW) {
            final InterfaceComponent buy = getBuyButton(slot);
            if (buy == null || !buy.interact(CREATE) || !Execution.delayUntil(
                () -> getOpenedScreen() == GrandExchange.Screen.SETUP_BUY_OFFER, 1200, 1800)) {
                return false;
            }
            screen = GrandExchange.Screen.SETUP_BUY_OFFER;
        }
        if (screen == GrandExchange.Screen.SETUP_BUY_OFFER) {
            if (!isSelected(itemName)) {
                if (!isSearchBoxOpen()) {
                    final InterfaceComponent search = Interfaces.newQuery().containers(GE_CONTAINER)
                        .types(InterfaceComponent.Type.SPRITE).actions("Choose item").results()
                        .first();
                    if (search == null || !search.interact("Choose item") ||
                        !Execution.delayUntil(OSRSGrandExchange::isSearchBoxOpen, 1800, 2400)) {
                        return false;
                    }
                }
                InterfaceComponent itemComp = getSearchedItemComponent(itemName);
                if (itemComp == null) {
                    final String searchTerm = getSearchTerm();
                    String continuation = null;
                    if (searchTerm != null && !searchTerm.isEmpty()) {
                        if (searchTerm.length() > itemName.length()) {
                            if (!clearEnteredTerm()) {
                                return false;
                            }
                        } else {
                            String itemNameBeginning = itemName.substring(0, searchTerm.length());
                            if (itemNameBeginning.equals(searchTerm)) {
                                continuation = itemName.substring(searchTerm.length());
                            } else {
                                if (!clearEnteredTerm()) {
                                    return false;
                                }
                            }
                        }
                    }
                    if (continuation == null) {
                        if (!Keyboard.type(itemName, false) ||
                            !Execution.delayUntil(() -> getSearchedItemComponent(itemName) != null,
                                1200, 1800
                            )) {
                            return false;
                        }
                    } else {
                        if (!Keyboard.type(continuation, false) ||
                            !Execution.delayUntil(() -> getSearchedItemComponent(itemName) != null,
                                1200, 1800
                            )) {
                            return false;
                        }
                    }
                    itemComp = getSearchedItemComponent(itemName);
                }
                if (itemComp == null || !itemComp.isVisible() ||
                    !Interfaces.scrollTo(itemComp, itemComp.getParentComponent())) {
                    return false;
                }
                if (!itemComp.interact("Select") ||
                    !Execution.delayUntil(() -> isSelected(itemName), 1200, 1800)) {
                    return false;
                }
            }
            if (!configureValuesAndConfirm(quantity, price)) {
                return false;
            }
        }
        return Execution.delayUntil(() -> {
            final GrandExchange.Offer offer = slot.getOffer();
            final ItemDefinition definition;
            return offer != null && (definition = offer.getItem()) != null &&
                definition.getName().equals(itemName);
        }, 1200, 1800);
    }

    private static InterfaceComponent getSearchedItemComponent(String item) {
        return Interfaces.newQuery().containers(162).types(InterfaceComponent.Type.SPRITE)
            .containedItem(ItemDefinition.getNamePredicate(item)).results().first();
    }

    private static boolean newSellOffer(
        GrandExchange.Slot slot, String item, int quantity,
        int price
    ) {
        GrandExchange.Screen screen = getOpenedScreen();
        Environment.getLogger().info("[GrandExchange] Current screen: " + screen);
        if (screen == GrandExchange.Screen.SETUP_BUY_OFFER) {
            Environment.getLogger().info("[GrandExchange] Returning to overview.");
            if (!returnToOverview()) {
                return false;
            }
            screen = GrandExchange.Screen.OVERVIEW;
        }
        if (screen == GrandExchange.Screen.OVERVIEW) {
            Environment.getLogger().info("[GrandExchange] Opening SELL screen.");
            final InterfaceComponent sell = getSellButton(slot);
            if (sell == null || !sell.interact(CREATE) || !Execution.delayUntil(
                () -> getOpenedScreen() == GrandExchange.Screen.SETUP_SELL_OFFER, 1200)) {
                return false;
            }
            screen = GrandExchange.Screen.SETUP_SELL_OFFER;
        }
        if (screen == GrandExchange.Screen.SETUP_SELL_OFFER) {
            if (!isSelected(item)) {
                Environment.getLogger().info("[GrandExchange] Selecting SpriteItem to sell.");
                final SpriteItem offer = Inventory.newQuery().names(item).results().first();
                Environment.getLogger().info("[GrandExchange] Item: " + offer);
                if (offer == null || !offer.interact("Offer") ||
                    !Execution.delayUntil(() -> isSelected(item), 1200, 1800)) {
                    return false;
                }
            }
            Environment.getLogger().info("[GrandExchange] Configuring values.");
            if (!configureValuesAndConfirm(quantity, price)) {
                return false;
            }
        }
        return Execution.delayUntil(() -> {
            final GrandExchange.Offer offer = slot.getOffer();
            final ItemDefinition definition;
            return offer != null && (definition = offer.getItem()) != null &&
                definition.getName().equals(item);
        }, 1200);
    }

    private static boolean configureValuesAndConfirm(int quantity, int price) {
        int initialQuantity = getOfferedQuantity();
        if (initialQuantity != quantity) {
            Environment.getLogger().info(
                "[GrandExchange] Changing the current quantity from " + initialQuantity + " to " +
                    quantity);
            if (!InputDialog.isOpen()) {
                final InterfaceComponent quantityButton = getEnterQuantityButton();
                Environment.getLogger()
                    .info("[GrandExchange] Opening enter quantity dialog via " + quantityButton);
                if (quantityButton == null || !quantityButton.interact("Enter quantity") ||
                    !Execution.delayUntil(InputDialog::isOpen, 1200, 1800)) {
                    return false;
                }
            }
            Environment.getLogger().info(
                "[GrandExchange] Attempting to enter the amount of " + quantity +
                    " into the enter amount dialog.");
            if (!InputDialog.enterAmount(quantity)) {
                Environment.getLogger().info(
                    "[GrandExchange] Failed to enter the quantity amount of " + quantity + ".");
                return false;
            } else if (!Execution.delayUntil(() -> getOfferedQuantity() == quantity, 1800, 2400)) {
                Environment.getLogger().info("[GrandExchange] The quantity amount of " + quantity +
                    " didn't update in time, the offered quantity is still " +
                    getOfferedQuantity() + ".");
                return false;
            }
        }
        final int selectedPrice = getOfferedPrice();
        Environment.getLogger().fine(
            String.format("[GrandExchange] Selected price is %s, we need %s", selectedPrice,
                price
            ));
        if (selectedPrice != price) {
            Environment.getLogger().info(
                "[GrandExchange] Changing the current quantity from " + selectedPrice + " to " +
                    price + ".");
            if (!InputDialog.isOpen()) {
                final InterfaceComponent priceButton = getEnterPriceButton();
                Environment.getLogger()
                    .info("[GrandExchange] Opening enter price dialog via " + priceButton);
                if (priceButton == null || !priceButton.interact("Enter price") ||
                    !Execution.delayUntil(InputDialog::isOpen, 1200, 1800)) {
                    return false;
                }
            }
            if (!InputDialog.enterAmount(price)) {
                Environment.getLogger()
                    .info("[GrandExchange] Failed to enter the price amount of " + price + ".");
                return false;
            } else if (!Execution.delayUntil(() -> getOfferedPrice() == price, 1800, 2400)) {
                Environment.getLogger().info("[GrandExchange] The price amount of " + price +
                    " didn't update in time, the offered price is still " + getOfferedPrice() +
                    ".");
                return false;
            }
        }
        return confirm();
    }

    private static boolean confirm() {
        final InterfaceComponent confirmButton =
            Interfaces.newQuery().containers(GE_CONTAINER).types(InterfaceComponent.Type.CONTAINER)
                .actions("Confirm").grandchildren(false).results().first();
        Environment.getLogger().info("[GrandExchange] Confirming the offer via " + confirmButton);
        return confirmButton != null && confirmButton.isVisible() &&
            confirmButton.interact("Confirm") &&
            Execution.delayUntil(() -> getOpenedScreen() == GrandExchange.Screen.OVERVIEW, 1200);
    }

    public static int getOfferedQuantity() {
        Varbit vb = Varbits.load(4396);
        return vb != null ? vb.getValue() : -1;
    }

    public static int getOfferedPrice() {
        Varbit vb = Varbits.load(4398);
        return vb != null ? vb.getValue() : -1;
    }

    private static InterfaceComponent getEnterQuantityButton() {
        return Interfaces.newQuery().containers(GE_CONTAINER).types(InterfaceComponent.Type.SPRITE)
            .actions("Enter quantity").results().first();
    }

    private static InterfaceComponent getEnterPriceButton() {
        return Interfaces.newQuery().containers(GE_CONTAINER).types(InterfaceComponent.Type.SPRITE)
            .actions("Enter price").results().first();
    }

    private static boolean clearEnteredTerm() {
        final int MAX_TRIES = Random.nextInt(3, 8);
        int tries = 0;
        int deleteKey = KeyEvent.VK_BACK_SPACE;
        String term = null;
        while (tries < MAX_TRIES && (term = getSearchTerm()) != null && !term.isEmpty()) {
            final int length = term.length();
            Keyboard.typeKey(deleteKey);
            if (length == term.length()) {
                if (deleteKey == KeyEvent.VK_BACK_SPACE) {
                    deleteKey = KeyEvent.VK_DELETE;
                } else {
                    deleteKey = KeyEvent.VK_BACK_SPACE;
                }
                ++tries;
            } else {
                tries = 0;
            }
        }
        return term == null || term.isEmpty();
    }

    private static boolean isSelected(String term) {
        //TODO rewrite this garbage
        final InterfaceComponent selected =
            Interfaces.newQuery().containers(GE_CONTAINER).types(InterfaceComponent.Type.LABEL)
                .visible().texts(term).results().first();
        return selected != null && selected.isVisible();
    }

    private static boolean isSearchBoxOpen() {
        return !Interfaces.newQuery().containers(162).types(InterfaceComponent.Type.LABEL)
            .grandchildren(false).texts(SEARCH_TERM).visible().results().isEmpty();
    }

    private static String getSearchTerm() {
        final InterfaceComponent searchTerm =
            Interfaces.newQuery().containers(162).types(InterfaceComponent.Type.LABEL)
                .grandchildren(false).texts(SEARCH_TERM).visible().results().first();
        if (searchTerm != null) {
            final Matcher matcher = SEARCH_TERM.matcher(searchTerm.getText());
            if (matcher.matches()) {
                return matcher.group("term");
            }
        }
        return null;
    }

    public static String getTitle() {
        final InterfaceComponent title = Interfaces.newQuery().containers(GE_CONTAINER).visible()
            .types(InterfaceComponent.Type.LABEL).textContains("Grand Exchange").results().first();
        return title == null ? null : title.getText();
    }

    public static boolean isOpen() {
        return getTitle() != null;
    }

    private static InterfaceComponentQueryResults getSlotComponents() {
        return Interfaces.newQuery().grandchildren(false).containers(GE_CONTAINER)
            .types(InterfaceComponent.Type.CONTAINER).visible().widths(115).heights(110).results();
    }

    private static InterfaceComponent getComponentForSlot(GrandExchange.Slot slot) {
        final InterfaceComponentQueryResults slots = getSlotComponents();
        if (slot.getIndex() < 0 || slot.getIndex() > slots.size()) {
            return null;
        }
        return slots.get(slot.getIndex());
    }

    private static String getOfferType() {
        final InterfaceComponent component =
            Interfaces.newQuery().containers(GE_CONTAINER).types(InterfaceComponent.Type.LABEL)
                .visible().texts("Buy offer", "Sell offer").results().first();
        return component == null ? null : component.getText();
    }

    public static GrandExchange.Screen getOpenedScreen() {
        final String title = getTitle();
        if (title == null) {
            return null;
        }
        if (title.equals("Grand Exchange")) {
            return GrandExchange.Screen.OVERVIEW;
        }
        final boolean buyOffer = Objects.equals("Buy offer", getOfferType());
        if (title.contains("Set up")) {
            return buyOffer ? GrandExchange.Screen.SETUP_BUY_OFFER :
                GrandExchange.Screen.SETUP_SELL_OFFER;
        }
        if (title.contains("Offer status")) {
            return buyOffer ? GrandExchange.Screen.ACTIVE_BUY_OFFER :
                GrandExchange.Screen.ACTIVE_SELL_OFFER;
        }
        return null;
    }

    private static InterfaceComponent getBuyButton(GrandExchange.Slot slot) {
        return getButton(slot, CREATE_BUY_OFFER);
    }

    private static InterfaceComponent getSellButton(GrandExchange.Slot slot) {
        return getButton(slot, CREATE_SELL_OFFER);
    }

    private static InterfaceComponent getButton(GrandExchange.Slot slot, String text) {
        final InterfaceComponent container = getComponentForSlot(slot);
        if (container != null) {
            return Interfaces.newQuery().containers(GE_CONTAINER)
                .provider(container::getChildren)
                .visible()
                .actions(text)
                .types(InterfaceComponent.Type.BOX)
                .results().first();
        }
        return null;
    }


    public static List<GrandExchange.Slot> getSlots(final Predicate<GrandExchange.Slot> predicate) {
        final long[] uids = OpenGrandExchangeOffer.getOffers();
        final ArrayList<GrandExchange.Slot> provider = new ArrayList<>();
        if (uids != null && uids.length > 0) {
            for (int i = 0; i < uids.length; i++) {
                final long uid = uids[i];
                if (uid == 0) {
                    continue;
                }
                provider.add(new GrandExchange.Slot(uid, i));
            }
        }
        provider.trimToSize();
        return Parallelize.collectToList(provider, predicate);
    }
}
