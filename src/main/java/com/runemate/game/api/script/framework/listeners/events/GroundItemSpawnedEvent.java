package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;

public class GroundItemSpawnedEvent implements Event {
    private final GroundItem groundItem;
    private final Coordinate coordinate;

    public GroundItemSpawnedEvent(GroundItem groundItem, Coordinate coordinate) {
        this.groundItem = groundItem;
        this.coordinate = coordinate;
    }

    public GroundItem getGroundItem() {
        return groundItem;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    @Override
    public String toString() {
        return "GroundItemSpawnedEvent(" + groundItem.toString() + ")";
    }
}
