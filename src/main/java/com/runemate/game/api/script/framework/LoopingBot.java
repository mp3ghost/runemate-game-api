package com.runemate.game.api.script.framework;

import static com.runemate.game.api.script.framework.AbstractBot.State.*;
import static com.runemate.game.api.script.framework.logger.BotLogger.Level.*;

import com.runemate.client.framework.open.*;
import com.runemate.game.api.client.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.hybrid.util.collections.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.game.api.script.framework.task.*;
import java.util.*;

/**
 * A looping implementation of AbstractBot. The majority of scripts should extend this.
 * Override onLoop for your main logic, and optionally override onStart, onStop,
 * onPause, and onResume for various things such as a GUI and event management.
 */
@SuppressWarnings("deprecation")
public abstract class LoopingBot extends AbstractBot {
    private static final int DEFAULT_MIN_LOOP_DELAY = 200, DEFAULT_MAX_LOOP_DELAY = 400,
        MAX_LOOP_DELAY = 60000;
    private int minLoopDelay = DEFAULT_MIN_LOOP_DELAY, maxLoopDelay = DEFAULT_MAX_LOOP_DELAY;

    /**
     * The script's entry point, all of your logic goes here.
     */
    public abstract void onLoop();

    @Override
    public final void run() {
        if (getMetaData().isLocal()) {
            List<EventListener> attachedListeners = getEventDispatcher().getListeners();
            if (this instanceof EventListener && !attachedListeners.contains(this)) {
                long listeners = Arrays.stream(getClass().getInterfaces())
                    .filter(aClass -> aClass.isAssignableFrom(EventListener.class)).count();
                getLogger().warn("[ALERT] We've detected that your main class has " + listeners
                    + " event listeners which have not been registered.\n"
                    + " To register them, execute getEventDispatcher().addListener(this) from AbstractBot#onStart(String... args) when you override it.");
            }
        }
        LoopingThread sdkTimeRegulator = (LoopingThread) getCache().get("SDK-3H Limiter");
        while (getState() != STOPPED && getState() != RESTARTING) {
            if (sdkTimeRegulator != null && sdkTimeRegulator.isInterrupted()) {
                ClientUI.showAlert(
                    SEVERE,
                    "The administration has been alerted to suspicious activities on your account."
                );
                ClientAlarms.onSecurityViolation(
                    "The SDK 3 hour timeout has been prematurely stopped by being interrupted",
                    Environment.getForumName()
                        + " bypassed the 3 hour SDK restriction running a bot called "
                        + getMetaData().getName() + " by " + getMetaData().getAuthor() + "."
                );
                stop("Stopped: SDK limit");
                continue;
            }
            if (getState() == State.PAUSED) {
                Execution.delay(200);
                continue;
            }
            Task gec = getGameEventController();
            final long targetLoopLength;
            long executionTime = System.currentTimeMillis();
            boolean event_validated = gec.validate();
            if (event_validated) {
                targetLoopLength =
                    (long) Random.nextGaussian(DEFAULT_MIN_LOOP_DELAY, DEFAULT_MAX_LOOP_DELAY);
            } else {
                targetLoopLength = (long) Random.nextGaussian(minLoopDelay, maxLoopDelay);
            }
            if (event_validated) {
                gec.execute();
            } else {
                onLoop();
            }
            executionTime = System.currentTimeMillis() - executionTime;
            long delay = Math.max(0, targetLoopLength - executionTime);
            if (delay > 0) {
                Execution.delay(delay);
            }
        }
    }

    /**
     * Gets a pair of the minimum loop delay (left) and the maximum loop delay (right).
     * The default is [25, 50]
     *
     * @return a pair of the delays in milliseconds
     */
    public final Pair<Integer, Integer> getLoopDelay() {
        return new Pair<>(minLoopDelay, maxLoopDelay);
    }

    /**
     * Sets the length of the delay between iterations (milliseconds)
     */
    public final void setLoopDelay(final int delay) {
        setLoopDelay(delay, delay);
    }

    /**
     * Sets the length of the delay between loops (milliseconds)
     */
    public final void setLoopDelay(final int minLoopDelay, final int maxLoopDelay) {
        if (minLoopDelay > MAX_LOOP_DELAY) {
            getLogger().warn(
                "minLoopDelay (" + minLoopDelay + ") exceeds the maximum allowed loop delay ("
                    + MAX_LOOP_DELAY + ") and has been reduced automatically");
        }
        if (maxLoopDelay > MAX_LOOP_DELAY) {
            getLogger().warn(
                "maxLoopDelay (" + maxLoopDelay + ") exceeds the maximum allowed loop delay ("
                    + MAX_LOOP_DELAY + ") and has been reduced automatically");
        }
        this.minLoopDelay = Math.min(minLoopDelay, MAX_LOOP_DELAY);
        this.maxLoopDelay = Math.min(maxLoopDelay, MAX_LOOP_DELAY);
    }
}
