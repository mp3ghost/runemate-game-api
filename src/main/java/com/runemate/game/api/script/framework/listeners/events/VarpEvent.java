package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.local.*;

public class VarpEvent implements Event {
    private final Varp varp;
    private final int oldValue;
    private final int newValue;

    public VarpEvent(Varp varp, int oldValue, int newValue) {
        this.varp = varp;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public Varp getVarp() {
        return varp;
    }

    public int getOldValue() {
        return oldValue;
    }

    public int getNewValue() {
        return newValue;
    }

    @Override
    public String toString() {
        return "VarpEvent{index=" + varp.getIndex() + ", " + oldValue + " -> " + newValue + "}";
    }
}
